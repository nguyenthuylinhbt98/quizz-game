var questions = [];
for(i=0; i<data.length; i++){
    questions.push(new Question(data[i]))
}
var quiz = new Quiz(questions);
var choices = new Array(quiz.length).fill(0);
// -------------------------

function Start(){
    $("#start").addClass("none");
    $(".content").removeClass("none");
    renderQuestion(quiz.length, 1);
}

function Choice(id, key){
    choices[id-1] = questions[id-1].answer[key];
    if(quiz.isSubmit(choices)){showSubmit()}
}

function Result(){
    event.preventDefault();
    ShowResult(quiz.getScore(choices));
}

