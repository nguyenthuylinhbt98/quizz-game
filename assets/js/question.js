function Question(obj) {
    this.id = obj.id;
    this.question = obj.question;
    this.answer = obj.answer;
    this.correct = obj.correct;
}

// Kiểm tra kết quả
Question.prototype.isCorrectAnswer = (answer)=>{
    return this.correct === answer;
}


