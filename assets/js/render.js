function ShowQues(idnone, idblock){
    var item = document.getElementById(idnone);
    item.remove();
    renderQuestion(quiz.length, idblock);
}

function showSubmit(){
    var result = document.getElementById("result");
    result.innerHTML=`<button onclick="Result()" id="btn-submit" type="submit" class="btn btn-danger float-left">Submit</button>`
}

function ShowResult(score){
    var btn = document.getElementById("btn-submit");
    btn.remove();
    var result = document.getElementById("result");
    result.innerHTML=`<h3>Score: ${score}/${quiz.length}</h3>`
}

function renderQuestion( TotalQues, id){
    var dataQues = questions[id-1];
    var form = document.getElementById("question");
    var divQues = document.createElement("div");
    divQues.setAttribute('id', dataQues.id);
    if(quiz.isSubmit(choices)){
        divQues.innerHTML = `
            <ul>
                <li>
                    <p><u><b>Question ${dataQues.id}:</b></u> ${dataQues.question}</p>
                </li>
                <li class=${questions[id-1].correct === dataQues.answer[0] ? "correct list-ans" : "list-ans"}>
                    <span>
                        <input type="radio" id="choice-A${dataQues.id}"
                            ${choices[id-1] === dataQues.answer[0] ? "checked" : ""}                        
                            name="answer-ques${dataQues.id}" 
                            onclick="Choice(${dataQues.id}, 0)"
                        ">
                        <label for="choice-A${dataQues.id}">A. ${dataQues.answer[0]}</label>
                    </span>
                </li>
                <li class=${questions[id-1].correct === dataQues.answer[1] ? "correct list-ans" : "list-ans"}>
                    <span>
                        <input type="radio" id="choice-B${dataQues.id}"
                            ${choices[id-1] === dataQues.answer[1] ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 1)"
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-B${dataQues.id}">B. ${dataQues.answer[1]}</label>
                    </span>
                </li>
                <li class=${questions[id-1].correct === dataQues.answer[2] ? "correct list-ans" : "list-ans"}>
                    <span>
                        <input type="radio" id="choice-C${dataQues.id}"
                            ${choices[id-1] === dataQues.answer[2] ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 2)"
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-C${dataQues.id}">C. ${dataQues.answer[2]}</label>
                    </span>
                </li>
                <li class=${questions[id-1].correct === dataQues.answer[3] ? "correct list-ans" : "list-ans"}>
                    <span>
                        <input type="radio" id="choice-D${dataQues.id}"
                            ${choices[id-1] === dataQues.answer[3] ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 3)" 
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-D${dataQues.id}">D. ${dataQues.answer[3]} </label>
                    </span>
                </li>
            </ul>
        `
    }else{
        divQues.innerHTML = `
            <ul>
                <li>
                    <p><u><b>Question ${dataQues.id}:</b></u> ${dataQues.question}</p>
                </li>
                <li class="list-ans">
                    <span>
                        <input type="radio" id="choice-A${dataQues.id}"
                            ${choices[id-1] !== 0 ? "checked" : ""}                        
                            name="answer-ques${dataQues.id}" 
                            onclick="Choice(${dataQues.id}, 0)"
                        ">
                        <label for="choice-A${dataQues.id}">A. ${dataQues.answer[0]}</label>
                    </span>
                </li>
                <li class="list-ans">
                    <span>
                        <input type="radio" id="choice-B${dataQues.id}"
                            ${choices[id-1] !== 0 ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 1)"
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-B${dataQues.id}">B. ${dataQues.answer[1]}</label>
                    </span>
                </li>
                <li class="list-ans">
                    <span>
                        <input type="radio" id="choice-C${dataQues.id}"
                            ${choices[id-1] !== 0 ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 2)"
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-C${dataQues.id}">C. ${dataQues.answer[2]}</label>
                    </span>
                </li>
                <li class="list-ans">
                    <span>
                        <input type="radio" id="choice-D${dataQues.id}"
                            ${choices[id-1] !== 0 ? "checked" : ""}
                            onclick="Choice(${dataQues.id}, 3)" 
                            name="answer-ques${dataQues.id}" 
                        ">
                        <label for="choice-D${dataQues.id}">D. ${dataQues.answer[3]} </label>
                    </span>
                </li>
            </ul>
        `
    }
    
    form.append(divQues);

    var pagination = document.getElementById('paginate');
    if(id == 1){
        pagination.innerHTML = ` <button onclick="ShowQues(${id}, ${id+1})" class="btn btn-primary">Next</button> `
    }

    if(id == TotalQues){
        pagination.innerHTML = ` <button onclick="ShowQues(${id}, ${id-1})" class="btn btn-primary">Previous</button> `
    }

    if(id > 1 && id < TotalQues){
        pagination.innerHTML = `
            <button onclick="ShowQues(${id}, ${id-1})" class="btn btn-primary">Previous</button>
            <button onclick="ShowQues(${id}, ${id+1})" class="btn btn-primary">Next</button>
        `
    }
}