var data = [
    {
    "id": 1,
    "question": "Find the sum of 111 + 222 + 333",
    "answer": ["700", "666", "10", "100"],
    "correct": "666", 
  }, {
    "id": 2,
    "question": "Subtract 457 from 832",
    "answer": ["375", "57", "376", "970"],
    "correct": "375",
  }, {
    "id": 3,
    "question": "50 times 5 is equal to",
    "answer": ["2500", "505", "500", "None of these"],
    "correct": "None of these",
  }, {
    "id": 4,
    "question": "90 ÷ 10",
    "answer": ["9", "10", "900", "1"],
    "correct": "9",
  }, {
    "id": 5,
    "question": "Simplify: 26 + 32 - 12",
    "answer": ["0", "32", "56", "46"],
    "correct": "46",
  }, {
    "id": 6,
    "question": "Find the product of 72 × 3",
    "answer": ["216", "7230", "106", "372"],
    "correct": "216",
  }, {
    "id": 7,
    "question": "Solve : 200 – (96 ÷ 4)",
    "answer": ["105", "176", "26", "16"],
    "correct": "176",
  }, {
    "id": 8,
    "question": "Solve : 24 + 4 ÷ 4",
    "answer": ["25", "6", "28", "7"],
    "correct": "25",
  }, {
    "id": 9,
    "question": "Simplify : 3 + 6 x (5 + 4) ÷ 3 - 7",
    "answer": ["11", "16", "14", "15"],
    "correct": "14",
  }, {
    "id": 10,
    "question": "Simplify :150 ÷ (6 + 3 x 8) - 5",
    "answer": ["2", "5", "0", "None of these"],
    "correct": "0",
  }, {
    "id": 11,
    "question": "How many cents is equal to $ ¼?",
    "answer": ["15", "20", "25", "30"],
    "correct": "25",
  }, {
    "id": 12,
    "question": "How many months are equal to 45 days?",
    "answer": ["1 ½ months.", "1 ¼ months", "¼ months.", " 2 ¼ months months."],
    "correct": "1 ½ months.",
  }, {
    "id": 13,
    "question": "How many diagonals are there in a quadrilateral?",
    "answer": ["2", "3", "4", "No diagonals."],
    "correct": "2",
  }, {
    "id": 14,
    "question": "Speed of a car is 60 km/hr. Distance covered in 1 ¼ hours is ………",
    "answer": ["60 km", "65 km", "70 km", "75 km"],
    "correct": "75 km",
  }, {
    "id": 15,
    "question": "If one side of a square is 35 m, then the area is …….",
    "answer": ["1252 m2", "1225 m2", "1252 m", "1225 m"],
    "correct": "1225 m2",
  }, {
    "id": 16,
    "question": "How many surfaces are there in a cube?",
    "answer": ["3", "4", "5", "None of these."],
    "correct": "None of these.",
  }, {
    "id": 17,
    "question": "How much water is added to 750 g milk to get 1 kilogram mixture of liquid?",
    "answer": [" 2.5 kg.", "0.25 kg.", "20.5 kg.", "25.0 kg."],
    "correct": "0.25 kg.",
  }, {
    "id": 18,
    "question": "An acute angle is …….",
    "answer": ["90 degree.", "less than 90 degree.", "more than 90 degree.", "None of these."],
    "correct": "less than 90 degree.",
  }, {
    "id": 19,
    "question": "How many parts are there in a triangle?",
    "answer": ["3", "6 times.", "9", "None of these."],
    "correct": "9",
  }, {
    "id": 20,
    "question": "Which unit is used to measure length and breadth?",
    "answer": ["Scale.", "Meter.", "Liter.", "Gram."],
    "correct": "Meter.",
  }]


